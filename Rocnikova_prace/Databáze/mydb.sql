-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Pon 04. dub 2016, 19:24
-- Verze serveru: 5.6.26
-- Verze PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `mydb`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `prispevky`
--

CREATE TABLE IF NOT EXISTS `prispevky` (
  `IdPrispevky` int(11) NOT NULL,
  `Nazev` varchar(45) DEFAULT NULL,
  `Text` varchar(45) DEFAULT NULL,
  `Id_uzivatel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `uzivatele`
--

CREATE TABLE IF NOT EXISTS `uzivatele` (
  `IdUzivatele` int(11) NOT NULL,
  `Jmeno` varchar(45) DEFAULT NULL,
  `Heslo` varchar(45) DEFAULT NULL,
  `Email` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `uzivatele`
--

INSERT INTO `uzivatele` (`IdUzivatele`, `Jmeno`, `Heslo`, `Email`) VALUES
(1, 'Svoboda875', 'svod78454', 'svoboda@seznam.cz'),
(2, 'Novak123', 'novak', 'novak@seznam.cz'),
(3, 'Stark_jar123', '123456789', 'stark@gmail.com');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `prispevky`
--
ALTER TABLE `prispevky`
  ADD PRIMARY KEY (`IdPrispevky`),
  ADD KEY `fk_Prispevky_Uzivatele_idx` (`Id_uzivatel`);

--
-- Klíče pro tabulku `uzivatele`
--
ALTER TABLE `uzivatele`
  ADD PRIMARY KEY (`IdUzivatele`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `prispevky`
--
ALTER TABLE `prispevky`
  MODIFY `IdPrispevky` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `uzivatele`
--
ALTER TABLE `uzivatele`
  MODIFY `IdUzivatele` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `prispevky`
--
ALTER TABLE `prispevky`
  ADD CONSTRAINT `fk_Prispevky_Uzivatele` FOREIGN KEY (`Id_uzivatel`) REFERENCES `uzivatele` (`IdUzivatele`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
